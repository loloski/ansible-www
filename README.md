# Ansible apache2

Configuration management for apache2

## Getting Started

Assumption your ansible inventory host was already in place and the host entry would be 

```
db
web
```
### Prerequisites

```
python 2.7 or 3.0
ansible 2.5
```

### Installing

For Ubuntu/Debian with latest ansible

```
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible
```

For MacOSX via homebrew

```
brew install ansible
```

Clone repository
```
git clone https://gitlab.com/loloski/ansible-www.git
```

## Authors

* **Ronaldo Chan** - *Initial work* - [loloski](https://gitlab.com/loloski/)

## License

This project is licensed under the MIT License

## Acknowledgments

* Tyrone for bring it up the topic on configuration management
